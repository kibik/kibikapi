﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kibik.Context.Migrations
{
    public partial class KibikApiv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Boards",
                table: "Tournaments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "MovementMethod",
                table: "Tournaments",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Tables",
                table: "Tournaments",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Boards",
                table: "Tournaments");

            migrationBuilder.DropColumn(
                name: "MovementMethod",
                table: "Tournaments");

            migrationBuilder.DropColumn(
                name: "Tables",
                table: "Tournaments");
        }
    }
}
