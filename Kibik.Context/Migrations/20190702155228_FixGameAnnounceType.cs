﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kibik.Context.Migrations
{
    public partial class FixGameAnnounceType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AnnounceTypeString",
                table: "GameAnnounces",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 2,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AnnounceTypeString",
                table: "GameAnnounces",
                maxLength: 2,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
