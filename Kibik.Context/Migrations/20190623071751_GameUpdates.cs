﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Kibik.Context.Migrations
{
    public partial class GameUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
               name: "CardType",
               table: "GameCards",
               newName: "CardTypeString");

            migrationBuilder.RenameColumn(
                name: "AnnounceType",
                table: "GameAnnounces",
                newName: "AnnounceTypeString");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
              name: "CardTypeString",
              table: "GameCards",
              newName: "CardType");

            migrationBuilder.RenameColumn(
                name: "AnnounceTypeString",
                table: "GameAnnounces",
                newName: "AnnounceType");
        }
    }
}
