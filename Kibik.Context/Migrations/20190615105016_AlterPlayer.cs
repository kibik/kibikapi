﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kibik.Context.Migrations
{
    public partial class AlterPlayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DateRegistered",
                table: "Players",
                newName: "DateUpdated");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "Players",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Players",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Players");

            migrationBuilder.RenameColumn(
                name: "DateUpdated",
                table: "Players",
                newName: "DateRegistered");
        }
    }
}
