﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Kibik.Context.Migrations
{
    public partial class KibikApiv3_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CardType",
                table: "GameCards",
                maxLength: 4,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "GameCards",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "GameCards",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "GameCards",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateCreated",
                table: "GameAnnounces",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateUpdated",
                table: "GameAnnounces",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "GameAnnounces",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "GameCards");

            migrationBuilder.DropColumn(
                name: "DateUpdated",
                table: "GameCards");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "GameCards");

            migrationBuilder.DropColumn(
                name: "DateCreated",
                table: "GameAnnounces");

            migrationBuilder.DropColumn(
                name: "DateUpdated",
                table: "GameAnnounces");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "GameAnnounces");

            migrationBuilder.AlterColumn<string>(
                name: "CardType",
                table: "GameCards",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 4,
                oldNullable: true);
        }
    }
}
