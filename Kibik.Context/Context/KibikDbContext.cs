﻿using Kibik.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Kibik.Context.Context
{
    public class KibikDbContext : DbContext
    {
        public KibikDbContext() : base()
        {
        }

        public DbSet<Club> Clubs { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<GameAnnounce> GameAnnounces { get; set; }

        public DbSet<GameCard> GameCards { get; set; }

        public DbSet<Pair> Pairs { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<PlayerClub> PlayerClubs { get; set; }

        public DbSet<Round> Rounds { get; set; }

        public DbSet<TableCard> TableCards { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Tournament> Tournaments { get; set; }

        public DbSet<TournamentManager> TournamentManagers { get; set; }

        public DbSet<TournamentPair> TournamentPairs { get; set; }

        public DbSet<TournamentTeam> TournamentTeams { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<GameTable> GameTables { get; set; } 

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Tournament Pair
            modelBuilder.Entity<TournamentPair>()
            .HasOne(tp => tp.Pair)
            .WithMany()
            .OnDelete(DeleteBehavior.Restrict)
            .IsRequired(true);
            #endregion

            #region Table
            modelBuilder.Entity<GameTable>()
                .HasOne(t => t.Location)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);
            #endregion

            #region Player

            modelBuilder.Entity<Player>()
                .HasIndex(p => p.Email)
                .IsUnique();

            modelBuilder.Entity<Pair>()
                .HasOne(g => g.Player1)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);
            modelBuilder.Entity<Pair>()
                .HasOne(g => g.Player2)
              .WithMany()
              .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            #endregion

            #region Game

            modelBuilder.Entity<Game>()
                .HasOne(g => g.NorthPlayer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            modelBuilder.Entity<Game>()
                .HasOne(g => g.EastPlayer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            modelBuilder.Entity<Game>()
                .HasOne(g => g.SouthPlayer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            modelBuilder.Entity<Game>()
                .HasOne(g => g.WestPlayer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            modelBuilder.Entity<Game>()
                .HasOne(g => g.Round)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired(true);

            //modelBuilder.Entity<Game>()
            //    .HasMany(g => g.GameCards)
            //    .WithOne(gc => gc.Game)
            //    .HasForeignKey(gc => gc.GameId);

            //modelBuilder.Entity<Game>()
            //    .HasMany(g => g.GameAnnounces)
            //    .WithOne(ga => ga.Game)
            //    .HasForeignKey(ga => ga.GameId);

            #endregion

            #region TeamPlayer
            modelBuilder.Entity<TeamPlayer>()
                .HasKey(tp => new { tp.TeamId, tp.PlayerId });
            modelBuilder.Entity<TeamPlayer>()
                .HasOne(tp => tp.Team)
                .WithMany(t => t.TeamPlayers)
                .HasForeignKey(tp => tp.TeamId);
            modelBuilder.Entity<TeamPlayer>()
                .HasOne(tp => tp.Player)
                .WithMany(p => p.TeamPlayers)
                .HasForeignKey(tp => tp.PlayerId);
            #endregion
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Server=DESKTOP-0CPM824\SQLEXPRESS;Database=KibikDatabase;Trusted_Connection=True;";// System.Environment.GetEnvironmentVariable("ConnectionStrings:KibikDb", System.EnvironmentVariableTarget.Process);
            optionsBuilder.UseSqlServer(connectionString);
        }

    }
}
