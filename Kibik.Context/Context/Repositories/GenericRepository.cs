﻿using Kibik.Context.Context.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Kibik.Context.Context.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private KibikDbContext database = null;
        DbSet<T> _objectSet;

        public GenericRepository(KibikDbContext database)
        {
            this.database = database;
            _objectSet = database.Set<T>();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate = null)
        {
            if(predicate != null)
            {
                return _objectSet.Where(predicate);
            }

            return _objectSet.AsQueryable<T>();
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _objectSet.FirstOrDefault(predicate);
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await _objectSet.FirstOrDefaultAsync(predicate);
        }

        public void Update(T entity)
        {
            database.Entry<T>(entity).State = EntityState.Modified;
        }

        public void Add(T entity)
        {
            _objectSet.Add(entity);
        }

        public void Attach(T entity)
        {
            _objectSet.Attach(entity);
        }

        public void Delete(T entity)
        {
            _objectSet.Remove(entity);
        }
    }
}
