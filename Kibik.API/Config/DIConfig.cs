﻿using Autofac;
using AzureFunctions.Autofac.Configuration;
using BoardMovements;
using Kibik.API.Services;
using Kibik.API.Services.Interfaces;
using Kibik.Context.Context;

namespace Kibik.API.Config
{
    public class DIConfig
    {
        public DIConfig(string functionName)
        {
            DependencyInjection.Initialize(builder =>
            {
                builder.RegisterType<UnitOfWork>().As<UnitOfWork>();
                builder.RegisterType<StorageService>().As<IStorageService>();
                builder.RegisterType<PairsMovementGenerator>().As<IPairsMovementGenerator>();
                builder.RegisterType<GameService>().As<IGameService>();
                builder.RegisterType<ResultsService>().As<IResultsService>();
            }, functionName);
        }
    }
}
