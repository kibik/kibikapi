﻿using System;

namespace Kibik.API.Models.Web.ViewModels
{
    public class GetImageSASViewModel
    {
        public string SAS { get; set; }
        public DateTimeOffset SharedAccessExpiryTime { get; set; }
    }
}
