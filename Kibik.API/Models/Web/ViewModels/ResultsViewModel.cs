﻿using Kibik.Entities.Models;
using System.Collections.Generic;

namespace Kibik.API.Models.Web.ViewModels
{
    public class ResultsViewModel
    {
        public int EastWestResult { get; set; }
        public int NorthSouthResult { get; set; }
        public string Attack { get; set; }
        public Direction Caller { get; set; }
        public List<GameAnnounce> GameAnnounces { get; set; }
        public List<GameCard> GameCards { get; set; }
    }
}
