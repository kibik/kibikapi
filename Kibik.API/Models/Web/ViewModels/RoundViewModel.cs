﻿using Kibik.Entities.Models;
using System.Collections.Generic;

namespace Kibik.API.Models.Web.ViewModels
{
    public class RoundViewModel
    {
        public int SequenceNumber { get; set; }

        public List<Game> FinishedGames { get; set; }
    }
}
