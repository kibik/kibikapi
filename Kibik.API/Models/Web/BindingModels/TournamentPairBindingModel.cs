﻿using Kibik.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kibik.API.Models.Web.BindingModels
{
    public class TournamentPairBindingModel
    {
        public int? TournamentId { get; set; }

        public Guid? PairId { get; set; }
    }

    public class UpdateTournamentPairBindingModel: TournamentPairBindingModel
    {
        public int Id { get; set; }
    }
}
