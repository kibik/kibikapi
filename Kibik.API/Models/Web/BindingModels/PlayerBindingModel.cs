﻿using Kibik.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kibik.API.Models.Web.BindingModels
{
    public class PlayerBindingModel: GeneralDbModel
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Country { get; set; }

        public DateTime BirthDate { get; set; }

        public string IBN { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int? ClubId { get; set; }
    }

    public class UpdatePlayerBindingModel: PlayerBindingModel
    {
        public Guid Id { get; set; }
    }
}
