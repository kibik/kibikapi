﻿namespace Kibik.API.Models.Web.BindingModels
{
    public class LocationBindingModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public int TablesCount { get; set; }
    }

    public class UpdateLocationBindingModel : LocationBindingModel
    {
        public int Id { get; set; }
    }
}
