﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kibik.API.Models.Web.BindingModels
{
    public class TableBindingModel
    {
        public string Name { get; set; }

        public int? LocationId { get; set; }

        public LocationBindingModel Location { get; set; }

        public int Sequence { get; set; }
    }

    public class UpdateTableBindingModel: TableBindingModel
    {
        public int TableId { get; set; }
    }
}
