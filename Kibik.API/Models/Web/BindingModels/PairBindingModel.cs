﻿using Kibik.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kibik.API.Models.Web.BindingModels
{
    public class PairBindingModel: GeneralDbModel
    {

        public string Name { get; set; }

        public Guid? PlayerId1 { get; set; }

        public PlayerBindingModel Player1 { get; set; }

        public Guid? PlayerId2 { get; set; }

        public PlayerBindingModel Player2 { get; set; }
    }

    public class UpdatePairBindingModel: PairBindingModel
    {
        public Guid Id { get; set; }
    }
}
