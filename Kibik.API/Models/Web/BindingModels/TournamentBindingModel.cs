﻿using Kibik.Entities.Models;
using System;

namespace Kibik.API.Models.Web.BindingModels
{
    public class TournamentBindingModel
    {
        public string Name { get; set; }
        public int? LocationId { get; set; }
        public LocationBindingModel Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsOpen { get; set; }
        public int Rounds { get; set; }
        public string Type { get; set; }

        public int Tables { get; set; }

        public int Boards { get; set; }

        public string MovementMethod { get; set; }

        public string Description { get; set; }
    }

    public class UpdateTournamentBindingModel : TournamentBindingModel
    {
        public int TournamentId { get; set; }
    }
}
