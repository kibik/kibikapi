﻿using System;

namespace Kibik.API.Models.Web.BindingModels
{
    public class ClubBindingModel
    {
        public string Name { get; set; }
        public int? LocationId { get; set; }
        public LocationBindingModel Location { get; set; }
        public DateTime DateEstablished { get; set; }
    }

    public class UpdateClubBindingModel : ClubBindingModel
    {
        public int ClubId { get; set; }
    }
}
