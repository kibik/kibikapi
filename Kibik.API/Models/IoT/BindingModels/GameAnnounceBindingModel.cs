﻿using Kibik.Entities.Models;
using System;

namespace Kibik.API.Models.IoT.BindingModels
{
    public class GameAnnounceBindingModel
    {
        public string AnnounceType { get; set; }
        public Direction Direction { get; set; }
        public Guid GameId { get; set; }

        /// <summary>
        /// The register difference presents the difference between the previous event and the current event in seconds
        /// </summary>
        public long RegisterDifference { get; set; }
    }

    public class UpdateGameAnnounceBindingModel: GameAnnounceBindingModel
    {
        public Guid AnnounceId { get; set; }
    }
}
      