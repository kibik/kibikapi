using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.Context.Context;
using Kibik.Entities.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace KibikApiv2
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class SignalR
    {
        [FunctionName("Test")]
        public static async Task<HttpResponseMessage> Run(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = "test/{name}")]HttpRequestMessage req,
        string name,
        ILogger log,
        [Inject] UnitOfWork unitOfWork
        )
            {
                log.LogInformation("C# HTTP trigger function processed a request.");

                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();

                // Set name to query string or body data
                name = name ?? data?.name;

                unitOfWork.Repository<Tournament>().Add(new Tournament() { Name = name, StartDate = DateTime.Now, EndDate = DateTime.Now });
                unitOfWork.SaveChanges();

                return name == null
                    ? req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a name on the query string or in the request body")
                    : req.CreateResponse(HttpStatusCode.OK, "Hello " + name);
            }   

    }
}
