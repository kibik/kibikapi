﻿using Kibik.API.Services.Interfaces;
using Kibik.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kibik.API.Services
{
    public class GameService : IGameService
    {
        public GameCard GetTrickWinner(GameAnnounceType contract, IList<GameCard> gameCards)
        {
            if (gameCards.Count > 4)
            {
                throw new Exception("Invalid game cards length.");
            }

            gameCards = gameCards.OrderBy(gc => gc.RegisteredAt).ToList();
            var startCard = gameCards.FirstOrDefault();
            GameCard winnerCard = startCard;
            for (int i = 1; i < gameCards.Count; i++)
            {
                if (gameCards[i].CardType.Type == startCard.CardType.Type)
                {
                    // 1. if cards have the same type and there is no trump card winning
                    // 2. if cars have the same type and they are trump
                    if (!IsCardTrump(contract, winnerCard.CardType) || (this.IsCardTrump(contract, gameCards[i].CardType) && this.IsCardTrump(contract, gameCards[i].CardType)))
                    {
                        winnerCard = gameCards[i].CardType > winnerCard.CardType ? gameCards[i] : winnerCard;
                    }
                    //it covers the case when the start card is not trump and there is a trump card winning but the current is not trump (not winning) =>
                    // => winner card is not changed

                }
                else
                {
                    // if there is winning trump card and the current card is also trump
                    if (IsCardTrump(contract, gameCards[i].CardType) && IsCardTrump(contract, winnerCard.CardType))
                    {
                        winnerCard = gameCards[i].CardType > winnerCard.CardType ? gameCards[i] : winnerCard;
                    }
                    //if the current card is trump but the winning card is not trump
                    else if (IsCardTrump(contract, gameCards[i].CardType) && !IsCardTrump(contract, winnerCard.CardType))
                    {
                        winnerCard = gameCards[i];
                    }
                }
            }

            return winnerCard;
        }

        private bool IsCardTrump(GameAnnounceType contract, GameCardType gameCard)
        {
            var announceType = contract.Type.ToString().ToLower();
            var gameCardType = gameCard.Type.ToString().ToLower();

            return announceType == gameCardType;
        }
    }
}
