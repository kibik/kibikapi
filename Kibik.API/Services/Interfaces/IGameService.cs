﻿using Kibik.Entities.Models;
using System.Collections.Generic;

namespace Kibik.API.Services.Interfaces
{
    public interface IGameService
    {
        GameCard GetTrickWinner(GameAnnounceType contract, IList<GameCard> gameCards);
    }
}
