﻿using Kibik.API.Models.Web.ViewModels;
using System;
using System.Threading.Tasks;

namespace Kibik.API.Services.Interfaces
{
    public interface IStorageService
    {
        Task<Uri> UploadBlobAsync(byte[] data, string contentType, string fileName);
        Task<GetImageSASViewModel> GetBlobSAS(Uri blobUrl);
    }
}
