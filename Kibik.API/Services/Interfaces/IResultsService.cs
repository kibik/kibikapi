﻿namespace Kibik.API.Services.Interfaces
{
    public interface IResultsService
    {
        /// <summary>
        /// Calculates end result
        /// </summary>
        /// <param name="tricks">Tricks taken</param>
        /// <param name="contract">End contract</param>
        /// <param name="isVulnerable">Wheather the pair is in vulnerability</param>
        int GetFinalResult(int tricks, string contract, bool isVulnerable, bool isDoubled, bool isRedoubled);
    }
}
