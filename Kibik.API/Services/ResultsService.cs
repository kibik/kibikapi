﻿using Kibik.API.Services.Interfaces;
using Kibik.Entities.Models;
using System;

namespace Kibik.API.Services
{
    public class ResultsService : IResultsService
    {
        public int GetFinalResult(int tricks, string contract, bool isVulnerable, bool isDoubled, bool isRedoubled)
        {
            var gameAnnounceType = new GameAnnounceType(contract);

            if (gameAnnounceType.Rank == null || gameAnnounceType.Type == AnnounceType.Pass)
            {
                return 0;
            }

            if (gameAnnounceType.Rank > (tricks - 6))
            {
                // undertricks
                var lostTricks = tricks - ((int)gameAnnounceType.Rank + 6);
                if (isVulnerable)
                {
                    if (isDoubled || isRedoubled)
                    {
                        var endResult = 0;
                        for (int i = 1; i <= Math.Abs(lostTricks); i++)
                        {
                            // 1st undertrick is 200
                            if (i == 1)
                            {
                                endResult -= 200;
                            }

                            // 2nd and each is subsequent is 300
                            if (i >= 2)
                            {
                                endResult -= 300;
                            }
                        }

                        if (isRedoubled)
                        {
                            endResult *= 2;
                        }

                        return endResult;
                    }

                    return lostTricks * 100;
                }
                else
                {
                    if (isDoubled || isRedoubled)
                    {
                        var endResult = 0;
                        for (int i = 1; i <= Math.Abs(lostTricks); i++)
                        {
                            // 1st undertrick is 200
                            if (i == 1)
                            {
                                endResult -= 100;
                            }

                            // 2nd and 3rd are 200, each
                            if (i >= 2 && i <= 3)
                            {
                                endResult -= 200;
                            }

                            // 4th and each subsequent
                            if (i >= 4)
                            {
                                endResult -= 300;
                            }
                        }

                        if (isRedoubled)
                        {
                            endResult *= 2;
                        }

                        return endResult;
                    }

                    return lostTricks * 50;
                }
            }
            else
            {
                var overtricks = tricks - 6;
                var score = 0;
                if (gameAnnounceType.Type == AnnounceType.Cl || gameAnnounceType.Type == AnnounceType.Di)
                {
                    score += 20 * overtricks;
                    score = CalculateWhenDoubled(score, isDoubled, isRedoubled);

                    // part-score bonus
                    if (gameAnnounceType.Rank < 5)
                    {
                        score += 50;
                    }
                    // game bonus
                    else if (gameAnnounceType.Rank == 5)
                    {
                        score += GetGameBonus(isVulnerable);
                    }
                    // small slam bonus
                    else if (gameAnnounceType.Rank == 6)
                    {
                        score += GetSmallSlamBonus(isVulnerable);
                    }
                    // big slam bonus
                    else if (gameAnnounceType.Rank == 7)
                    {
                        score += GetBigSlamBonus(isVulnerable);
                    }

                    return score;
                }
                else if (gameAnnounceType.Type == AnnounceType.He || gameAnnounceType.Type == AnnounceType.Sp)
                {
                    score += 30 * overtricks;
                    score = CalculateWhenDoubled(score, isDoubled, isRedoubled);

                    // part-score bonus
                    if (gameAnnounceType.Rank < 4)
                    {
                        score += 50;
                    }
                    // game bonus
                    else if (gameAnnounceType.Rank == 4)
                    {
                        score += GetGameBonus(isVulnerable);
                    }
                    // small slam bonus
                    else if (gameAnnounceType.Rank == 6)
                    {
                        score += GetSmallSlamBonus(isVulnerable);
                    }
                    // big slam bonus
                    else if (gameAnnounceType.Rank == 7)
                    {
                        score += GetBigSlamBonus(isVulnerable);
                    }

                    return score;
                }
                else
                {
                    score += 40; // first overtrick in NT is 40 points
                    score = CalculateWhenDoubled(score, isDoubled, isRedoubled);
                    score += CalculateWhenDoubled(30 * (overtricks - 1), isDoubled, isRedoubled);

                    // part-score bonus
                    if (gameAnnounceType.Rank < 3)
                    {
                        score += 50;
                    }
                    // game bonus
                    else if (gameAnnounceType.Rank == 3)
                    {
                        score += GetGameBonus(isVulnerable);
                    }
                    // small slam bonus
                    else if (gameAnnounceType.Rank == 6)
                    {
                        score += GetSmallSlamBonus(isVulnerable);
                    }
                    // big slam bonus
                    else if (gameAnnounceType.Rank == 7)
                    {
                        score += GetBigSlamBonus(isVulnerable);
                    }

                    return score;
                }
            }
        }

        private int GetGameBonus(bool isVulnerable)
        {
            if (isVulnerable)
            {
                return 500;
            }
            else
            {
                return 300;
            }
        }

        private int GetSmallSlamBonus(bool isVulnerable)
        {
            if (isVulnerable)
            {
                return 750;
            }
            else
            {
                return 500;
            }
        }

        private int GetBigSlamBonus(bool isVulnerable)
        {
            if (isVulnerable)
            {
                return 1500;
            }
            else
            {
                return 1000;
            }
        }

        private int CalculateWhenDoubled(int score, bool isDoubled, bool isRedoubled)
        {
            if (isDoubled)
            {
                return score * 2;
            }

            if (isRedoubled)
            {
                return score * 4;
            }

            return score;
        }
    }
}