﻿using Kibik.API.Models.Web.ViewModels;
using Kibik.API.Services.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Kibik.API.Services
{
    public class StorageService : IStorageService
    {
        private readonly CloudStorageAccount _cloudStorageAccount;
        private readonly CloudBlobClient _cloudBlobClient;

        public StorageService()
        {
            if (_cloudStorageAccount == null || _cloudBlobClient == null)
            {
                this._cloudStorageAccount = CloudStorageAccount
                    .Parse(Environment.GetEnvironmentVariable("AzureWebJobsStorage"));
                this._cloudBlobClient = _cloudStorageAccount.CreateCloudBlobClient();
            }
        }
        public async Task<Uri> UploadBlobAsync(byte[] data, string contentType, string fileName)
        {
            CloudBlobContainer container = _cloudBlobClient.GetContainerReference("images");

            await container.CreateIfNotExistsAsync(
              BlobContainerPublicAccessType.Container,
              new BlobRequestOptions(),
              new OperationContext());
            CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
            blob.Properties.ContentType = contentType;

            var accessCondition = new AccessCondition() {
                IfMatchETag= blob.Properties.ETag
            };

            await blob.UploadFromByteArrayAsync(data, 0, data.Length, accessCondition, null, null).ConfigureAwait(false);

            return blob.Uri;
        }

        public async Task<GetImageSASViewModel> GetBlobSAS(Uri blobUrl)
        {
            ICloudBlob blob = await _cloudBlobClient.GetBlobReferenceFromServerAsync(blobUrl);

            var expiryTime = new DateTimeOffset(DateTime.UtcNow.AddHours(2));
            var sasViewModel = new GetImageSASViewModel()
            {
                SAS = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy()
                {
                    SharedAccessExpiryTime = expiryTime,
                    Permissions = SharedAccessBlobPermissions.Read
                }),
                SharedAccessExpiryTime = expiryTime,
            };

            return sasViewModel;
        }
    }
}
