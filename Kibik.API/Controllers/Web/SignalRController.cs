﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web
{
    [Disable]
    public class SignalRController
    {
        private const string HubName = "websocket";
        [FunctionName("negotiate")]
        public static SignalRConnectionInfo SignalRInfo(
        [HttpTrigger(AuthorizationLevel.Anonymous)] HttpRequestMessage req,
        [SignalRConnectionInfo(HubName = HubName, UserId = "{Query.UserId}")] SignalRConnectionInfo connectionInfo)
        {
            return connectionInfo;
        }

        [FunctionName("message")]
        public static Task SendMessage(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "message/{groupName}")] HttpRequestMessage req,
        string groupName,
        [SignalR(HubName = HubName)] IAsyncCollector<SignalRMessage> signalRMessages)
        {
            object message = req.Content.ReadAsAsync<object>();
            return signalRMessages.AddAsync(
                        new SignalRMessage
                        {
                            Target = "newMessage",
                            GroupName = groupName,
                            Arguments = new[] { message }
                        }
                    );
        }

        [FunctionName("subscribeToGroup")]
        public static Task SubscribeToGroup(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "subscribe/group/{groupName}/user/{userId}")] HttpRequestMessage req,
        string groupName,
        string userId,
        [SignalR(HubName = HubName)] IAsyncCollector<SignalRGroupAction> singalRGroupActions)
        {
            return singalRGroupActions.AddAsync(new SignalRGroupAction
            {
                UserId = userId,
                GroupName = groupName,
                Action = GroupAction.Add
            });
        }

        [FunctionName("unsubscribeFromGroup")]
        public static Task UnsubscribeFromGroup(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "unsubscribe/group/{groupName}/user/{userId}")] HttpRequestMessage req,
        string groupName,
        string userId,
        [SignalR(HubName = HubName)] IAsyncCollector<SignalRGroupAction> singalRGroupActions)
        {
            return singalRGroupActions.AddAsync(new SignalRGroupAction
            {
                UserId = userId,
                GroupName = groupName,
                Action = GroupAction.Remove
            });
        }
    }
}
