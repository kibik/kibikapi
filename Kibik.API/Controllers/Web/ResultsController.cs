﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.API.Models.Web.ViewModels;
using Kibik.Context.Context;
using Kibik.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class ResultsController
    {
        /// <summary>
        /// Get a player by id
        /// </summary>
        [FunctionName("GetResults")]
        public static async Task<IActionResult> GetResults(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "results/{tournamentId}")]HttpRequest req,
           int tournamentId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var rounds = unitOfWork.Repository<Round>()
                .GetAll(r => r.TournamentId == tournamentId)
                .OrderByDescending(r => r.SequenceNumber);

            if (rounds == null)
            {
                return new NotFoundObjectResult("No rounds found with the supplied tournament id.");
            }

            Game game = null;
            foreach (var round in rounds)
            {
                game = await unitOfWork.Repository<Game>()
                    .GetAll(g => g.RoundId == round.Id && g.IsFinished)
                    .Include(g => g.GameAnnounces)
                    .Include(g => g.GameCards)
                    .LastOrDefaultAsync();

                if (game != null)
                {
                    break;
                }
            }
            

            if (game == null)
            {
                return new NotFoundObjectResult("Game Not Found!");
            }

            var resultsViewModel = new ResultsViewModel()
            {
                NorthSouthResult = game.NorthSouthResult,
                EastWestResult = game.EastWestResult,
                Attack = game.Attack,
                Caller = game.Caller,
                GameAnnounces = game.GameAnnounces,
                GameCards = game.GameCards
            };

            return new OkObjectResult(resultsViewModel);
        }
    }
}
