﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.API.Services.Interfaces;
using Kibik.Context.Context;
using Kibik.Entities.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public class TournamentController
    {
        /// <summary>
        /// Get a list of all tournaments
        /// </summary>
        [FunctionName("GetAllTournaments")]
        public static async Task<HttpResponseMessage> GetAllTournaments(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "tournament/{hasImages:bool?}")]HttpRequestMessage req,
            bool? hasImages,
            ILogger log,
            [Inject] UnitOfWork unitOfWork,
            [Inject] IStorageService storageService
        )
        {
            var allTournaments = unitOfWork.Repository<Tournament>()
                .GetAll(t => !t.IsDeleted)
                .ToList();

            if (hasImages != null || hasImages == true)
            {
                foreach (var tournament in allTournaments)
                {
                    if (tournament.Logo == null)
                    {
                        continue;
                    }

                    var blobSAS = await storageService.GetBlobSAS(new Uri(tournament.Logo));
                    tournament.Logo += blobSAS.SAS;
                }
            }

            return req.CreateResponse(HttpStatusCode.OK, allTournaments);
        }

        /// <summary>
        /// Get a tournament by id
        /// </summary>
        [FunctionName("GetTournament")]
        public static async Task<HttpResponseMessage> GetTournament(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "tournament/{tournamentId}/{hasImages:bool?}")]HttpRequestMessage req,
            int tournamentId,
            bool? hasImages,
            ILogger log,
            [Inject] UnitOfWork unitOfWork,
            [Inject] IStorageService storageService
        )
        {
            var tournament = await unitOfWork.Repository<Tournament>()
                .GetAsync(t => t.Id == tournamentId && !t.IsDeleted);

            if ((hasImages != null || hasImages == true) && tournament.Logo != null)
            {
                var blobSAS = await storageService.GetBlobSAS(new Uri(tournament.Logo));
                tournament.Logo += blobSAS.SAS;
            }

            return req.CreateResponse(HttpStatusCode.OK, tournament);
        }
    }
}
