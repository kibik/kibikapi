﻿using AzureFunctions.Autofac;
using BoardMovements;
using Kibik.API.Config;
using Kibik.Context.Context;
using Kibik.API.Models.Web.BindingModels;
using Kibik.API.Services.Interfaces;
using Kibik.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web.Admin
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class TournamentManagerController
    {
        // Waiting for update of packages to be able to run
        //[FunctionName("Function1")]
        //public static void Run([BlobTrigger("tournaments/{name}", Connection = "AzureWebJobsStorage")]Stream myBlob, string name, TraceWriter log)
        //{
        //    log.Info($"C# Blob trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");
        //}

        /// <summary>
        /// Upload an image to Azure blob storage and attach it to existing tournament
        /// </summary>
        [FunctionName("Admin_AddTournamentImage")]
        public static async Task<HttpResponseMessage> AddTournamentImage(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/tournament/{tournamentId}/image")]HttpRequestMessage req,
           int tournamentId,
           ILogger log,
           [Inject] UnitOfWork unitOfWork,
           [Inject] IStorageService storageService
        )
        {
            var tournament = await unitOfWork.Repository<Tournament>()
                .GetAsync(t => t.Id == tournamentId);

            if (tournament == null)
            {
                log.LogError("Tournament not found!");
                return req.CreateResponse(HttpStatusCode.NotFound);
            }

            var data = await req.Content.ReadAsByteArrayAsync();
          
            if (data == null)
            {
                log.LogError("File is not attached.");
                return req.CreateResponse(HttpStatusCode.BadRequest);
            }

            var fileName = $"tournamament_{tournamentId}.jpg";
            Uri uri = await storageService.UploadBlobAsync(data, "image/*", fileName);

            tournament.Logo = uri.ToString();
            tournament.LogoFileName = fileName;

            await unitOfWork.SaveChangesAsync();

            var blobSAS = await storageService.GetBlobSAS(new Uri(tournament.Logo));
            var result = tournament.Logo + blobSAS.SAS;

            return req.CreateResponse(HttpStatusCode.Created, result);
        }

        /// <summary>
        /// Get an image from Azure blob storage by tournament id
        /// </summary>
        [FunctionName("Admin_GetTournamentImage")]
        public static async Task<IActionResult> GetTournamentImage(
          [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/tournament/{tournamentId}/image")]HttpRequest req,
          int tournamentId,
          ILogger log,
          [Inject] UnitOfWork unitOfWork,
          [Inject] IStorageService storageService
       )
        {
            var tournament = await unitOfWork.Repository<Tournament>()
                .GetAsync(t => t.Id == tournamentId);

            if (tournament == null)
            {
                log.LogError("Tournament Not Found!");
                return new NotFoundResult();
            }

            if(tournament.Logo == null)
            {
                return new OkObjectResult(null);
            }

            var blobSAS = await storageService.GetBlobSAS(new Uri(tournament.Logo));
            var result = tournament.Logo + blobSAS.SAS;
            return new OkObjectResult(result);
        }

        /// <summary>
        /// Add a tournament to database
        /// </summary>
        [FunctionName("AddTournament")]
        public static async Task<HttpResponseMessage> AddTournament(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/tournament")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var tournamentBindingModel = await req.Content.ReadAsAsync<TournamentBindingModel>();

            Location location = await unitOfWork.Repository<Location>().GetAsync(l => l.Id == tournamentBindingModel.LocationId);

            if (location == null)
            {
                log.LogError("Location not found!");
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            var tournament = new Tournament()
            {
                Name = tournamentBindingModel.Name,
                Location = location,
                StartDate = tournamentBindingModel.StartDate,
                EndDate = tournamentBindingModel.EndDate,
                IsOpen = tournamentBindingModel.IsOpen,
                Rounds = tournamentBindingModel.Rounds,
                Type = (TournamentType)int.Parse(tournamentBindingModel.Type),
                Tables = tournamentBindingModel.Tables,
                Boards = tournamentBindingModel.Boards,
                MovementMethod = tournamentBindingModel.MovementMethod,
                Description = tournamentBindingModel.Description,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            unitOfWork.Repository<Tournament>().Add(tournament);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, tournament.Id);
        }

        /// <summary>
        /// Update saved tournament data
        /// </summary>
        [FunctionName("UpdateTournament")]
        public static async Task<HttpResponseMessage> UpdateTournament(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "manage/tournament")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var tournamentBindingModel = await req.Content.ReadAsAsync<UpdateTournamentBindingModel>();

            var tournamentRepository = unitOfWork.Repository<Tournament>();
            var tournament = await tournamentRepository.GetAsync(t => t.Id == tournamentBindingModel.TournamentId);
            if (tournament == null)
            {
                log.LogError("Tournament not found!");
                return req.CreateResponse(HttpStatusCode.NotFound, "Tournament not found");
            }

            Location location = await unitOfWork.Repository<Location>().GetAsync(l => l.Id == tournamentBindingModel.LocationId);

            if (location == null)
            {
                log.LogError("Location not found!");
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            tournament.Name = tournamentBindingModel.Name;
            tournament.Location = location;
            tournament.StartDate = tournamentBindingModel.StartDate;
            tournament.EndDate = tournamentBindingModel.EndDate;
            tournament.IsOpen = tournamentBindingModel.IsOpen;
            tournament.Rounds = tournamentBindingModel.Rounds;
            tournament.Tables = tournamentBindingModel.Tables;
            tournament.Boards = tournamentBindingModel.Boards;
            tournament.MovementMethod = tournamentBindingModel.MovementMethod;
            tournament.Type = (TournamentType)int.Parse(tournamentBindingModel.Type);
            tournament.Description = tournamentBindingModel.Description;
            tournament.DateUpdated = DateTime.UtcNow;

            tournamentRepository.Update(tournament);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific tournament
        /// </summary>
        [FunctionName("DeleteTournament")]
        public static async Task<HttpResponseMessage> DeleteTournament(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "manage/tournament/{tournamentId}")]HttpRequestMessage req,
            int tournamentId,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
            )
        {
            var tournamentRepository = unitOfWork.Repository<Tournament>();
            var tournament = await tournamentRepository.GetAsync(t => t.Id == tournamentId);
            if (tournament == null)
            {
                log.LogError("Tournament not found!");
                return req.CreateResponse(HttpStatusCode.NotFound, "Tournament not found");
            }

            tournament.DateUpdated = DateTime.UtcNow;
            tournament.IsDeleted = true;

            tournamentRepository.Update(tournament);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Add a pair to database. If player id is specified to be null, a new player can be created
        /// </summary>
        [FunctionName("AddPair")]
        public static async Task<HttpResponseMessage> AddPair(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/pair")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var pairBindingModel = await req.Content.ReadAsAsync<PairBindingModel>();
            Player player1 = null;
            if (pairBindingModel.PlayerId1 != null && pairBindingModel.Player1 != null)
            {
                player1 = new Player
                {
                    IBN = pairBindingModel.Player1.IBN,
                    FirstName = pairBindingModel.Player1.FirstName,
                    MiddleName = pairBindingModel.Player1.MiddleName,
                    LastName = pairBindingModel.Player1.LastName,
                    BirthDate = pairBindingModel.Player1.BirthDate,
                    Password = pairBindingModel.Player1.Password,
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow,
                    Email = pairBindingModel.Player1.Email,
                    Country = pairBindingModel.Player1.Country
                };


            }
            else
            {
                player1 = await unitOfWork.Repository<Player>().GetAsync(p => p.Id == pairBindingModel.PlayerId1);
                if (player1 == null)
                {
                    return req.CreateResponse(HttpStatusCode.NotFound, "Player 1 was not found!");
                }
            }

            Player player2 = null;
            if (pairBindingModel.PlayerId2 != null && pairBindingModel.Player2 != null)
            {
                player1 = new Player
                {
                    IBN = pairBindingModel.Player2.IBN,
                    FirstName = pairBindingModel.Player2.FirstName,
                    MiddleName = pairBindingModel.Player2.MiddleName,
                    LastName = pairBindingModel.Player2.LastName,
                    BirthDate = pairBindingModel.Player2.BirthDate,
                    Password = pairBindingModel.Player2.Password,
                    DateUpdated = DateTime.UtcNow,
                    DateCreated = DateTime.UtcNow,
                    Email = pairBindingModel.Player2.Email,
                    Country = pairBindingModel.Player2.Country
                };


            }
            else
            {
                player2 = await unitOfWork.Repository<Player>().GetAsync(p => p.Id == pairBindingModel.PlayerId2);
                if (player2 == null)
                {
                    return req.CreateResponse(HttpStatusCode.NotFound, "Player 2 was not found!");
                }
            }

            var pair = new Pair
            {
                Name = pairBindingModel.Name,
                Player1 = player1,
                Player2 = player2,
            };

            unitOfWork.Repository<Pair>().Add(pair);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, pair.Id);
        }

        /// <summary>
        /// Get a list of all pairs.  Name, Offset, Length of the list can be specified
        /// </summary>
        [FunctionName("ListPairs")]
        public static IActionResult ListPairs(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/pair")]HttpRequest req,
        ILogger log,
        [Inject] UnitOfWork unitOfWork
)
        {
            string name = req.Query["Name"].Count > 0 ? req.Query["Name"][0] : null;
            var offset = req.Query["Offset"].Count > 0 ? int.Parse(req.Query["Offset"]) : 0;
            var length = req.Query["Length"].Count > 0 ? int.Parse(req.Query["Length"]) : 10;
            var pairs = unitOfWork.Repository<Pair>()
                .GetAll(p => (name == null || p.Name.Equals(name, StringComparison.OrdinalIgnoreCase)))
                .Skip(offset * length)
                .Take(length)
                .Include(p => p.Player1)
                .Include(p => p.Player2)
                .ToList();

            return new OkObjectResult(pairs);
        }

        /// <summary>
        /// Get a list of all pairs in a given tournament.  Name, Offset, Length of the list can be specified
        /// </summary>
        [FunctionName("ListTournamentPairs")]
        public static IActionResult ListTournamentPairs(
        [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/tournament/{tournamentId}/pair")]HttpRequest req,
        int tournamentId,
        ILogger log,
        [Inject] UnitOfWork unitOfWork
)
        {
            string name = req.Query["Name"].Count > 0 ? req.Query["Name"][0] : null;
            var offset = req.Query["Offset"].Count > 0 ? int.Parse(req.Query["Offset"]) : 0;
            var length = req.Query["Length"].Count > 0 ? int.Parse(req.Query["Length"]) : 10;
            var tournamentPairs = unitOfWork.Repository<TournamentPair>()
                .GetAll(tp => tp.TournamentId == tournamentId)
                .Skip(offset * length)
                .Take(length)
                .Include(tp => tp.Pair)
                .ThenInclude(p => p.Player1)
                .Include(tp => tp.Pair)
                .ThenInclude(p => p.Player2)
                .ToList();

            return new OkObjectResult(tournamentPairs);
        }

        /// <summary>
        /// Get a pair by id
        /// </summary>
        [FunctionName("GetPair")]
        public static async Task<IActionResult> GetPair(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/pair/{pairId}")]HttpRequest req,
            string pairId,
            [Inject] UnitOfWork unitOfWork
            )
        {
            Guid pairIdAsGuid = Guid.Parse(pairId);
            var pair = await unitOfWork.Repository<Pair>().GetAsync(p => p.Id == pairIdAsGuid);
            if (pair == null)
            {
                return new NotFoundObjectResult("Pair not found!");
            }
            return new OkObjectResult(pair);
        }

        /// <summary>
        /// Add a tournament pair to database. Attach an existing pair to existing tournament
        /// </summary>
        [FunctionName("AddTournamentPair")]
        public static async Task<HttpResponseMessage> AddTournamentPair(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/tournament/pair")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var tournamentPairBindingModel = await req.Content.ReadAsAsync<TournamentPairBindingModel>();

            if (tournamentPairBindingModel.TournamentId != null && tournamentPairBindingModel.PairId != null)
            {
                Tournament tournament = await unitOfWork.Repository<Tournament>()
                    .GetAsync(t => t.Id == tournamentPairBindingModel.TournamentId && !t.IsDeleted);

                if (tournament == null)
                {
                    log.LogError("Tournament not found!");
                    return req.CreateResponse(HttpStatusCode.NotFound, "Tournament not found!");
                }

                Pair pair = await unitOfWork.Repository<Pair>().GetAsync(p => p.Id == tournamentPairBindingModel.PairId);
                if (pair == null)
                {
                    log.LogError("Pair not found!");
                    return req.CreateResponse(HttpStatusCode.NotFound, "Pair not found!");
                }

                var tournamentPair = new TournamentPair
                {
                    Pair = pair,
                    Tournament = tournament
                };

                unitOfWork.Repository<TournamentPair>().Add(tournamentPair);
                await unitOfWork.SaveChangesAsync();

                return req.CreateResponse(HttpStatusCode.Created, tournamentPair.Id);
            }
            else
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Please provide TournamentId and PairId");
            }
        }

        /// <summary>
        /// Generate rounds for a specific tournament depending on number of defined tables, rounds and pairs
        /// </summary>
        [FunctionName("GenerateRounds")]
        public static async Task<HttpResponseMessage> GenerateRounds(
        [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/generateRounds/{tournamentId:int}")]HttpRequestMessage req,
        ILogger log,
        int tournamentId,
        [Inject] UnitOfWork unitOfWork,
        [Inject] IPairsMovementGenerator movementGenerator)
        {

            var tournamentRepository = unitOfWork.Repository<Tournament>();
            var tournament = await tournamentRepository.GetAsync(t => t.Id == tournamentId);
            if (tournament == null)
            {
                log.LogError("Tournament not found!");
                return req.CreateResponse(HttpStatusCode.NotFound, "Tournament not found");
            }

            var tablesRepository = unitOfWork.Repository<GameTable>();
            var tournamentLocationTables = tablesRepository
                .GetAll(t => t.LocationId == tournament.LocationId)
                .OrderBy(t => t.LocationId)
                .ToArray();


            int gamesCount = tournament.Rounds;// TODO: for now fixed
            var roundRepository = unitOfWork.Repository<Round>();
            Round[] rounds = new Round[tournament.Rounds];
            for (int i = 0; i < tournament.Rounds; i++)
            {
                var round = new Round { Tournament = tournament, SequenceNumber = i + 1, GamesCount = gamesCount };
                rounds[i] = round;
                roundRepository.Add(round);
            }

            var tournamentPairsRepository = unitOfWork.Repository<TournamentPair>();
            var tournamentPairs = tournamentPairsRepository
                .GetAll(tp => tp.TournamentId == tournament.Id)
                .Include(tp => tp.Pair)
                .Include(tp => tp.Pair.Player1)
                .Include(tp => tp.Pair.Player2)
                .ToArray();

            var games = movementGenerator.GenerateSingleWinnerMovement(tournamentLocationTables, rounds, tournamentPairs);

            var gamesRepository = unitOfWork.Repository<Game>();
            foreach (var game in games)
            {
                gamesRepository.Add(game);
            }

            unitOfWork.SaveChanges();

            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}
