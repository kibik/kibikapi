﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.Context.Context;
using Kibik.API.Models.Web.BindingModels;
using Kibik.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Kibik.API.Models.Web.ViewModels;

namespace Kibik.API.Controllers.Web.Admin
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class GeneralAdminController
    {
        #region Club
        /// <summary>
        /// Add a club to database
        /// </summary>
        [FunctionName("AddClub")]
        public static async Task<HttpResponseMessage> AddClub(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/club")]HttpRequestMessage req,
           ILogger log,
           [Inject] UnitOfWork unitOfWork
       )
        {
            var clubBindingModel = await req.Content.ReadAsAsync<ClubBindingModel>();

            Location location = await unitOfWork.Repository<Location>().GetAsync(l => l.Id == clubBindingModel.LocationId);

            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            var club = new Club
            {
                Name = clubBindingModel.Name,
                Location = location,
                DateEstablished = clubBindingModel.DateEstablished,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            unitOfWork.Repository<Club>().Add(club);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, club.Id);
        }


        /// <summary>
        /// Update saved club data
        /// </summary>
        [FunctionName("UpdateClub")]
        public static async Task<HttpResponseMessage> UpdateClub(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "manage/club")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var clubBindingModel = await req.Content.ReadAsAsync<UpdateClubBindingModel>();

            var clubRepository = unitOfWork.Repository<Club>();
            var club = await clubRepository.GetAsync(c => c.Id == clubBindingModel.ClubId);
            if (club == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Club not found");
            }

            Location location = await unitOfWork.Repository<Location>().GetAsync(l => l.Id == clubBindingModel.LocationId);

            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }


            club.Name = clubBindingModel.Name;
            club.Location = location;
            club.DateEstablished = clubBindingModel.DateEstablished;
            club.DateUpdated = DateTime.UtcNow;

            clubRepository.Update(club);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific club
        /// </summary>
        [FunctionName("DeleteClub")]
        public static async Task<HttpResponseMessage> DeleteTournament(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "manage/club/{clubId:int}")]HttpRequestMessage req,
            int clubId,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
            )
        {

            var clubRepository = unitOfWork.Repository<Club>();
            var club = await clubRepository.GetAsync(c => c.Id == clubId);
            if (club == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Club not found");
            }

            club.DateUpdated = DateTime.UtcNow;
            club.IsDeleted = true;

            clubRepository.Update(club);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Get a club by id
        /// </summary>
        [FunctionName("GetClub")]
        public static async Task<HttpResponseMessage> GetClub(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/club/{clubId:int}")]HttpRequestMessage req,
           int clubId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var club = await unitOfWork.Repository<Club>().GetAsync(c => c.Id == clubId);

            if (club == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Club not found");
            }

            return req.CreateResponse(HttpStatusCode.OK, club);
        }

        /// <summary>
        /// Get a list of all clubs. Name, Offset, Length of the list can be specified
        /// </summary>
        [FunctionName("ListClubs")]
        public static IActionResult ListClubs(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/club")]HttpRequest req,
           [Inject] UnitOfWork unitOfWork
)
        {
            string name = req.Query["Name"].Count > 0 ? req.Query["Name"][0] : null;
            var offset = req.Query["Offset"].Count > 0 ? int.Parse(req.Query["Offset"]) : 0;
            var length = req.Query["Length"].Count > 0 ? int.Parse(req.Query["Length"]) : 10;
            var clubs = unitOfWork.Repository<Club>()
                .GetAll(c => (name == null || c.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && !c.IsDeleted))
                .Skip(offset * length)
                .Take(length)
                .ToList();

            return new OkObjectResult(clubs);
        }

        #endregion

        #region Location

        /// <summary>
        /// Add a location to database
        /// </summary>
        [FunctionName("AddLocation")]
        public static async Task<HttpResponseMessage> AddLocation(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/location")]HttpRequestMessage req,
           ILogger log,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var locationBindingModel = await req.Content.ReadAsAsync<LocationBindingModel>();

            var location = new Location
            {
                Name = locationBindingModel.Name,
                Address = locationBindingModel.Address,
                City = locationBindingModel.City,
                Country = locationBindingModel.Country,
                PostCode = locationBindingModel.PostCode,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            unitOfWork.Repository<Location>().Add(location);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, location.Id);
        }

        /// <summary>
        /// Update saved location data
        /// </summary>
        [FunctionName("UpdateLocation")]
        public static async Task<HttpResponseMessage> LocationClub(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "manage/location")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var locationBindingModel = await req.Content.ReadAsAsync<UpdateLocationBindingModel>();

            var locationRepository = unitOfWork.Repository<Location>();
            var location = await locationRepository.GetAsync(l => l.Id == locationBindingModel.Id && !l.IsDeleted);
            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            location.Name = locationBindingModel.Name;
            location.Address = locationBindingModel.Address;
            location.City = locationBindingModel.City;
            location.Country = locationBindingModel.Country;
            location.DateUpdated = DateTime.UtcNow;

            locationRepository.Update(location);

            var gameTablesRepository = unitOfWork.Repository<GameTable>();
            var gameTables = gameTablesRepository
                .GetAll(gt => gt.LocationId == location.Id && !gt.IsDeleted)
                .OrderBy(gt => gt.Sequence)
                .ToArray();

            int currentTablesCount = gameTables.Length;
            if (locationBindingModel.TablesCount != currentTablesCount)
            {
                int biggestOfTheTableCounts = currentTablesCount > locationBindingModel.TablesCount ? currentTablesCount : locationBindingModel.TablesCount;
                for(int i = 1; i <= biggestOfTheTableCounts; i++)
                {
                    bool shouldDelete = i <= currentTablesCount && i > locationBindingModel.TablesCount;
                    bool shouldAdd = i <= locationBindingModel.TablesCount && i > currentTablesCount;
                    if(shouldAdd)
                    {
                        gameTablesRepository.Add(new GameTable
                        {
                            LocationId = location.Id,
                            Name = $"Table {i}",
                            Sequence = i,
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow
                        });
                    } else if(shouldDelete)
                    {
                        gameTablesRepository.Delete(gameTables[i - 1]);
                    }
                } 
            }

            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific location
        /// </summary>
        [FunctionName("DeleteLocation")]
        public static async Task<HttpResponseMessage> DeleteLocation(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "manage/location/{locationId:int}")]HttpRequestMessage req,
            int locationId,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
            )
        {
            var locationRepository = unitOfWork.Repository<Location>();
            var location = await locationRepository.GetAsync(l => l.Id == locationId);
            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            location.DateUpdated = DateTime.UtcNow;
            location.IsDeleted = true;

            locationRepository.Update(location);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete specific locations
        /// </summary>
        [FunctionName("DeleteLocations")]
        public static async Task<HttpResponseMessage> DeleteLocations(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "manage/location")]HttpRequestMessage req,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
            )
        {
            var locationRepository = unitOfWork.Repository<Location>();
            var test = await req.Content.ReadAsStringAsync();
            var locationsBindingModel = await req.Content.ReadAsAsync<IList<UpdateLocationBindingModel>>();
            if (locationsBindingModel == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Locations are not attached");
            }

            foreach (var locationBindingModel in locationsBindingModel)
            {
                var location = await locationRepository.GetAsync(l => l.Id == locationBindingModel.Id);
                if (location == null)
                {
                    return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
                }

                location.DateUpdated = DateTime.UtcNow;
                location.IsDeleted = true;
                locationRepository.Update(location);
            }
            
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Get a list of all locations. City, Offset, Length of the list can be specified
        /// </summary>
        [FunctionName("ListLocations")]
        public static IActionResult ListLocations(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/location")]HttpRequest req,
           [Inject] UnitOfWork unitOfWork
        )
        {
            string city = req.Query["City"].Count > 0 ? req.Query["City"][0] : null;
            var offset = req.Query["Offset"].Count > 0 ? int.Parse(req.Query["Offset"]) : 0;
            var length = req.Query["Length"].Count > 0 ? int.Parse(req.Query["Length"]) : 10;
            var locations = unitOfWork.Repository<Location>()
                .GetAll(l => (city == null || l.City.Equals(city, StringComparison.OrdinalIgnoreCase) && !l.IsDeleted))
                .Skip(offset * length)
                .Take(length)
                .ToList();

            return new OkObjectResult(locations);
        }

        /// <summary>
        /// Get a location by id
        /// </summary>
        [FunctionName("GetLocation")]
        public static async Task<HttpResponseMessage> GetLocation(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/location/{locationId:int}")]HttpRequestMessage req,
           int locationId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var location = await unitOfWork.Repository<Location>()
                .GetAll(l => l.Id == locationId && !l.IsDeleted)
                .FirstOrDefaultAsync();
            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            var locationTablesCount = await unitOfWork.Repository<GameTable>()
                .GetAll(gt => gt.LocationId == location.Id && !gt.IsDeleted)
                .CountAsync();

            LocationViewModel locationViewModel = new LocationViewModel
            {
                Id = location.Id,
                Name = location.Name,
                Address = location.Address,
                City = location.City,
                Country = location.Country,
                PostCode = location.PostCode,
                TablesCount = locationTablesCount
            };

            return req.CreateResponse(HttpStatusCode.OK, locationViewModel);
        }


        #endregion

        #region Table

        /// <summary>
        /// Add a table to database
        /// </summary>
        [FunctionName("AddTable")]
        public static async Task<HttpResponseMessage> AddTable(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "manage/table")]HttpRequestMessage req,
           [Inject] UnitOfWork unitOfWork)
        {
            var tableBindingModel = await req.Content.ReadAsAsync<TableBindingModel>();

            Location location = await unitOfWork.Repository<Location>().GetAsync(l => l.Id == tableBindingModel.LocationId);

            if (location == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Location not found");
            }

            var table = new GameTable
            {
                Name = tableBindingModel.Name,
                Sequence = tableBindingModel.Sequence,
                Location = location,
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now,
                IsDeleted = false
            };

            unitOfWork.Repository<GameTable>().Add(table);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, table.Id);
        }

        /// <summary>
        /// Get a table by id
        /// </summary>
        [FunctionName("GetTable")]
        public static async Task<HttpResponseMessage> GetTable(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "manage/table/{tableId:int}")]HttpRequestMessage req,
            int tableId,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
        )
        {
            var tableRespository = unitOfWork.Repository<GameTable>();
            var table = await tableRespository.GetAsync(t => t.Id == tableId);
            if (table == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Table not found");
            }

            return req.CreateResponse(HttpStatusCode.OK, table);
        }

        /// <summary>
        /// Delete a specific table
        /// </summary>
        [FunctionName("DeleteTable")]
        public static async Task<HttpResponseMessage> DeleteTable(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "manage/table/{tableId:int}")]HttpRequestMessage req,
            int tableId,
            ILogger log,
            [Inject] UnitOfWork unitOfWork
            )
        {
            var tableRespository = unitOfWork.Repository<GameTable>();
            var table = await tableRespository.GetAsync(t => t.Id == tableId);
            if (table == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Table not found");
            }

            table.DateUpdated = DateTime.UtcNow;
            table.IsDeleted = true;

            tableRespository.Update(table);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        #endregion
    }
}
