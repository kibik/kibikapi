﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.Context.Context;
using Kibik.API.Models.Web.BindingModels;
using Kibik.Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class PlayerController
    {
        /// <summary>
        /// Create a player 
        /// </summary>
        [FunctionName("Register")]
        public static async Task<HttpResponseMessage> Register(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "player")]HttpRequestMessage req,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var playerBindingModel = await req.Content.ReadAsAsync<PlayerBindingModel>();

            var player = new Player
            {
                FirstName = playerBindingModel.FirstName,
                MiddleName = playerBindingModel.MiddleName,
                LastName = playerBindingModel.LastName,
                Email = playerBindingModel.Email,
                IBN = playerBindingModel.IBN,
                BirthDate = playerBindingModel.BirthDate,
                Country = playerBindingModel.Country,
                ClubId = playerBindingModel.ClubId,
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow                
            };

            unitOfWork.Repository<Player>().Add(player);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.Created, player.Id);
        }

        /// <summary>
        /// Update saved player data
        /// </summary>
        [FunctionName("UpdatePlayer")]
        public static async Task<HttpResponseMessage> UpdatePlayer(
           [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "player")]HttpRequestMessage req,
           [Inject] UnitOfWork unitOfWork
)
        {
            var updatePlayerBindingModel = await req.Content.ReadAsAsync<UpdatePlayerBindingModel>();
            var playerRepository = unitOfWork.Repository<Player>();
            var player = await playerRepository.GetAsync(p => p.Id == updatePlayerBindingModel.Id);
            if (player == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Player not found");
            }

            player.FirstName = updatePlayerBindingModel.FirstName;
            player.MiddleName = updatePlayerBindingModel.MiddleName;
            player.LastName = updatePlayerBindingModel.LastName;
            player.Email = updatePlayerBindingModel.Email;
            player.IBN = updatePlayerBindingModel.IBN;
            player.BirthDate = updatePlayerBindingModel.BirthDate;
            player.Country = updatePlayerBindingModel.Country;

            unitOfWork.Repository<Player>().Update(player);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific player
        /// </summary>
        [FunctionName("DeletePlayer")]
        public static async Task<HttpResponseMessage> DeletePlayer(
           [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "player/{playerId}")]HttpRequestMessage req,
           string playerId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var palyerIdAsGuid = Guid.Parse(playerId);
            var playerRepository = unitOfWork.Repository<Player>();
            var player = await playerRepository.GetAsync(p => p.Id == palyerIdAsGuid);
            if (player == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Player not found");
            }


            unitOfWork.Repository<Player>().Delete(player);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Get a lisf of all players. Country, Offset, Length of the list can be specified
        /// </summary>
        [FunctionName("ListPlayers")]
        public static IActionResult ListPlayers(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "player")]HttpRequest req,
           [Inject] UnitOfWork unitOfWork
        )
        {
            string country = req.Query["Country"].Count > 0 ? req.Query["Country"][0] : null;
            var offset = req.Query["Offset"].Count > 0 ? int.Parse(req.Query["Offset"]) : 0;
            var length = req.Query["Length"].Count > 0 ? int.Parse(req.Query["Length"]) : 10;
            var players = unitOfWork.Repository<Player>()
                .GetAll(p => (country == null || p.Country.Equals(country, StringComparison.OrdinalIgnoreCase)))
                .Skip(offset * length)
                .Take(length)
                .ToList();

            return new OkObjectResult(players);
        }

        /// <summary>
        /// Get a player by id
        /// </summary>
        [FunctionName("GetPlayer")]
        public static async Task<IActionResult> GetPlayer(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "player/{playerId}")]HttpRequest req,
           string playerId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var palyerIdAsGuid = Guid.Parse(playerId);
            var player = await unitOfWork.Repository<Player>().GetAsync(p => p.Id == palyerIdAsGuid);
            if(player == null)
            {
                return new NotFoundObjectResult("Player Not Found!");
            }

            return new OkObjectResult(player);
        }
    }
}
