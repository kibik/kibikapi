﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.API.Models.Web.ViewModels;
using Kibik.Context.Context;
using Kibik.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.Web
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class GameController
    {
        /// <summary>
        /// Get a game by id
        /// </summary>
        [FunctionName("GetGame")]
        public static async Task<HttpResponseMessage> GetGame(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "game/{gameId}")]HttpRequestMessage req,
            string gameId,
            [Inject] UnitOfWork unitOfWork
            )
        {
            Guid gameIdAsGuid = Guid.Parse(gameId);
            var game = await unitOfWork.Repository<Game>()
                .GetAll(g => g.Id == gameIdAsGuid)
                .Include(g => g.GameAnnounces)
                .Include(g => g.GameCards)
                .Include(g => g.Board)
                .Include(g => g.NorthPlayer)
                .Include(g => g.EastPlayer)
                .Include(g => g.SouthPlayer)
                .Include(g => g.WestPlayer)
                .FirstOrDefaultAsync();

            var gameViewModel = new GameViewModel()
            {
                //TODO: add properties
            };
            return req.CreateResponse(HttpStatusCode.OK, game);
        }

        /// <summary>
        /// Get All Finished Games By TournamentId
        /// </summary>
        [FunctionName("GetAllFinishedGamesByTournamentId")]
        public static async Task<IActionResult> GetAllFinishedGamesByTournamentId(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "game/tournament/{tournamentId}")]HttpRequestMessage req,
            int tournamentId,
            [Inject] UnitOfWork unitOfWork
            )
        {
            var rounds = unitOfWork.Repository<Round>()
                .GetAll(r => r.TournamentId == tournamentId)
                .OrderBy(r => r.SequenceNumber);

            if (rounds == null)
            {
                return new NotFoundObjectResult("No rounds found with the supplied tournament id.");
            }

            List<RoundViewModel> roundViewModels = new List<RoundViewModel>();
            foreach (var round in rounds)
            {
                var finishedGames = await unitOfWork.Repository<Game>()
                    .GetAll(g => g.RoundId == round.Id && g.IsFinished)
                    .Include(g => g.GameAnnounces)
                    .Include(g => g.GameCards)
                    .Include(g => g.Board)
                    .Include(g => g.NorthPlayer)
                    .Include(g => g.EastPlayer)
                    .Include(g => g.SouthPlayer)
                    .Include(g => g.WestPlayer)
                    .ToListAsync();

                if (finishedGames != null)
                {
                    var roundViewModel = new RoundViewModel()
                    {
                        SequenceNumber = round.SequenceNumber,
                        FinishedGames = finishedGames
                    };

                    roundViewModels.Add(roundViewModel);
                }
            }

            return new OkObjectResult(roundViewModels);
        }
    }
}
