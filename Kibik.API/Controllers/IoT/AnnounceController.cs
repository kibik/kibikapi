﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.Context.Context;
using Kibik.API.Models.IoT.BindingModels;
using Kibik.Entities.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.IoT
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class AnnounceController
    {
        //[FunctionName("AddGameAnnounce")]
        //public static async Task<HttpResponseMessage> Run(
        //   [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "announce")]HttpRequestMessage req,
        //   ILogger log,
        //   [Inject] UnitOfWork unitOfWork
        //   )
        //{
        //    // Get request body
        //    var announceBindingModel = await req.Content.ReadAsAsync<GameAnnounceBindingModel>();

        //    await SaveGameAnnounce(unitOfWork, announceBindingModel);
        //    await unitOfWork.SaveChangesAsync();

        //    return req.CreateResponse(HttpStatusCode.OK);
        //}

        //[FunctionName("AddGameAnnounceList")]
        //public static async Task<HttpResponseMessage> AddGameCardList(
        //   [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "announce")]HttpRequestMessage req,
        //   ILogger log,
        //   [Inject] UnitOfWork unitOfWork
        //)
        //{
        //    // Get request body
        //    var announceBindingModels = await req.Content.ReadAsAsync<List<GameAnnounceBindingModel>>();

        //    foreach (var announceBindingModel in announceBindingModels)
        //    {
        //        var result = await SaveGameAnnounce(unitOfWork, announceBindingModel);
        //        if (result != null)
        //        {
        //            return result;
        //        }
        //    }

        //    await unitOfWork.SaveChangesAsync();

        //    return req.CreateResponse(HttpStatusCode.OK);
        //}

        //private async static Task<HttpResponseMessage> SaveGameAnnounce(UnitOfWork unitOfWork, GameAnnounceBindingModel announceBindingModel)
        //{
        //    var game = await unitOfWork.Repository<Game>().GetAsync(g => g.Id == announceBindingModel.GameId);

        //    if (game == null)
        //    {
        //        HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
        //        // we guess that initially all games are created
        //        return httpRequestMessage.CreateResponse(HttpStatusCode.NotFound, "Game not Found");
        //    }

        //    var player = await unitOfWork.Repository<Player>()
        //        .GetAsync(p => p.Id == announceBindingModel.PlayerId);
        //    if (player == null)
        //    {
        //        HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
        //        return httpRequestMessage.CreateResponse(HttpStatusCode.NotFound, "Player not found");
        //    }

        //    var newCard = new GameCard()
        //    {
        //        CardType = announceBindingModel.AnnounceType,
        //        GameId = announceBindingModel.GameId,
        //        Direction = announceBindingModel.Direction,
        //        SequenceNumber = game.GameAnnounces.Count + 1,
        //        Player = player,
        //        RegisteredAt = DateTimeOffset.Now.AddSeconds(announceBindingModel.RegisterDifference)
        //    };

        //    game.GameCards.Add(newCard);
        //    unitOfWork.Repository<GameCard>().Add(newCard);

        //    return null;
        //}
    }
}
