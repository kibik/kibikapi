﻿using AzureFunctions.Autofac;
using Kibik.API.Config;
using Kibik.API.Models.IoT.BindingModels;
using Kibik.API.Services.Interfaces;
using Kibik.Context.Context;
using Kibik.Entities.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Kibik.API.Controllers.IoT
{
    [DependencyInjectionConfig(typeof(DIConfig))]
    public static class GameController
    {
        private const int DeckCount = 52;

        /// <summary>
        /// Get an active game depending on the table
        /// </summary>
        [FunctionName("GetTableActiveGame")]
        public static async Task<HttpResponseMessage> GetTableActiveGame(
           [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "game/table/{tableId:int}")]HttpRequestMessage req,
           int tableId,
           [Inject] UnitOfWork unitOfWork
           )
        {
            var tableActiveGame = await unitOfWork.Repository<Game>()
                .GetAll(g => g.GameTableId == tableId && g.GameCards.Count(gc => !gc.IsDeleted) < 52)
                .OrderBy(g => g.Round.SequenceNumber)
                .ThenBy(g => g.SequenceNumber)
                .Include(g => g.Board)
                .Include(g => g.Table)
                .FirstOrDefaultAsync();
            if (tableActiveGame == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound);
            }

            return req.CreateResponse(HttpStatusCode.OK, tableActiveGame.Id);
        }

        #region GameCard
        /// <summary>
        /// Add a list of game cards to specific game
        /// </summary>
        [FunctionName("AddGameCard")]
        public static async Task<HttpResponseMessage> AddGameCard(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "game/card")]HttpRequestMessage req,
           ILogger log,
           [Inject] UnitOfWork unitOfWork,
           [Inject] IResultsService resultsService,
            [Inject] IGameService gameService
        )
        {
            var gameBindingModels = await req.Content.ReadAsAsync<List<GameCardBindingModel>>();

            var isGameFinished = false;
            foreach (var gameBindingModel in gameBindingModels)
            {
                try
                {
                    var gameCard = await SaveGameCard(unitOfWork, gameBindingModel);

                    if (gameCard.SequenceNumber == 1)
                    {
                        var game = await unitOfWork.Repository<Game>()
                            .GetAsync(g => g.Id == gameBindingModel.GameId);
                        game.Attack = gameCard.CardTypeString;
                    }

                    isGameFinished = gameCard.SequenceNumber == DeckCount;

                    if (isGameFinished)
                    {
                        var game = await unitOfWork.Repository<Game>()
                            .GetAll(g => g.Id == gameBindingModel.GameId)
                            .Include(g => g.GameCards)
                            .Include(g => g.Board)
                            .FirstOrDefaultAsync();

                        var northSouthTricks = GetNorthSouthTricks(game, gameService);

                        game.NorthSouthTricks = northSouthTricks;
                        game.EastWestTricks = 13 - northSouthTricks;

                        //TODO: add checking for double or re-double
                        if (game.Caller == Direction.N || game.Caller == Direction.S)
                        {
                            var result = resultsService.GetFinalResult(game.NorthSouthTricks, game.Contract, game.Board.IsNorthSouthInZone, false, false);
                            game.NorthSouthResult = result;
                            game.EastWestResult = -result;
                        }
                        else
                        {
                            var result = resultsService.GetFinalResult(game.EastWestTricks, game.Contract, game.Board.IsEastWestInZone, false, false);
                            game.NorthSouthResult = -result;
                            game.EastWestResult = result;
                        }

                        game.IsFinished = true;
                    }
                }
                catch (ArgumentException ex)
                {
                    return req.CreateErrorResponse(HttpStatusCode.NotFound, ex.Message);
                }
            }

            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK, isGameFinished);
        }

        /// <summary>
        /// Update saved game card data
        /// </summary>
        [FunctionName("UpdateGameCard")]
        public static async Task<HttpResponseMessage> UpdateGameCard(
           [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "game/card")]HttpRequestMessage req,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var updateGameCardBindingModel = await req.Content.ReadAsAsync<UpdateGameCardBindingModel>();

            var card = await unitOfWork.Repository<GameCard>()
                .GetAsync(gc => gc.Id == updateGameCardBindingModel.CardId);

            card.DateUpdated = DateTime.UtcNow;
            card.Direction = updateGameCardBindingModel.Direction;
            card.CardTypeString = updateGameCardBindingModel.CardType;

            unitOfWork.Repository<GameCard>().Update(card);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific game card
        /// </summary>
        [FunctionName("DeleteGameCard")]
        public static async Task<HttpResponseMessage> DeleteGameCard(
           [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "game/card/{cardId}")]HttpRequestMessage req,
           string cardId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            Guid cardIdAsGuid = Guid.Parse(cardId);
            var card = await unitOfWork.Repository<GameCard>()
                .GetAsync(gc => gc.Id == cardIdAsGuid);
            card.IsDeleted = true;
            card.DateUpdated = DateTime.UtcNow;

            unitOfWork.Repository<GameCard>().Update(card);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        private static int GetNorthSouthTricks(Game game, IGameService gameService)
        {
            var cards = game.GameCards
                .OrderBy(gc => gc.SequenceNumber)
                .ToList();

            var contract = new GameAnnounceType(game.Contract);
            var northSouthTricks = 0;
            for (int i = 0; i < 52; i += 4)
            {
                var trick = new List<GameCard>();
                trick.Add(cards[i]);
                trick.Add(cards[i + 1]);
                trick.Add(cards[i + 2]);
                trick.Add(cards[i + 3]);

                var winner = gameService.GetTrickWinner(contract, trick);
                if (winner.Direction == Direction.N || winner.Direction == Direction.S)
                {
                    northSouthTricks += 1;
                }
            }

            return northSouthTricks;
        }

        private async static Task<GameCard> SaveGameCard(UnitOfWork unitOfWork, GameCardBindingModel gameBindingModel)
        {
            //TODO: refactor. Game is taken from the database for every single card
            var game = await unitOfWork.Repository<Game>()
                .GetAll(g => g.Id == gameBindingModel.GameId)
                .Include(g => g.GameCards)
                .FirstOrDefaultAsync();

            if (game == null)
            {
                throw new ArgumentException("Game not found.");
            }

            var playerId = GetPlayerIdByDirection(game, gameBindingModel.Direction);
            if (playerId == Guid.Empty)
            {
                throw new ArgumentException("Player not found.");
            }

            var player = await unitOfWork.Repository<Player>()
                .GetAsync(p => p.Id == playerId);
            if (player == null)
            {
                throw new ArgumentException("Player not found.");
            }

            var newCard = new GameCard()
            {
                CardTypeString = gameBindingModel.CardType,
                Direction = gameBindingModel.Direction,
                SequenceNumber = game.GameCards.Count + 1,
                Player = player,
                RegisteredAt = DateTimeOffset.Now.AddSeconds(-gameBindingModel.RegisterDifference),
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            game.GameCards.Add(newCard);
            unitOfWork.Repository<Game>().Update(game);

            return newCard;
        }

        #endregion

        #region GameAnnounce
        /// <summary>
        /// Add a list of game announces to specific game
        /// </summary>
        [FunctionName("AddGameAnnounce")]
        public static async Task<HttpResponseMessage> AddGameAnnounce(
           [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "game/announce")]HttpRequestMessage req,
           ILogger log,
           [Inject] UnitOfWork unitOfWork
        )
        {
            // Get request body
            var gameBindingModels = await req.Content.ReadAsAsync<List<GameAnnounceBindingModel>>();

            if (gameBindingModels.Count == 0)
            {
                req.CreateResponse(HttpStatusCode.BadRequest);
            }

            bool isFinalAnnounce = false;
            foreach (var gameBindingModel in gameBindingModels)
            {
                var announces = await SaveGameAnnounce(unitOfWork, gameBindingModel);

                isFinalAnnounce = announces
                    .OrderBy(a => a.SequenceNumber)
                    .TakeLast(3)
                    .All(a => a.AnnounceType.Type == AnnounceType.Pass) && announces.Count >= 3;

                if (isFinalAnnounce)
                {
                    //TODO: add checking for double or re-double
                    var game = await unitOfWork.Repository<Game>()
                                        .GetAsync(g => g.Id == gameBindingModel.GameId);

                    var finalAnnounce = announces
                        .OrderBy(a => a.SequenceNumber)
                        .TakeLast(4)
                        .FirstOrDefault();

                    game.Caller = finalAnnounce.Direction;
                    game.Contract = finalAnnounce.AnnounceTypeString;
                }
            }

            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK, isFinalAnnounce);
        }

        /// <summary>
        /// Update saved game announce data
        /// </summary>
        /// <param name="req"></param>
        /// <param name="unitOfWork"></param>
        /// <returns></returns>
        [FunctionName("UpdateGameAnnounce")]
        public static async Task<HttpResponseMessage> UpdateGameAnnounce(
           [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "game/announce")]HttpRequestMessage req,
           [Inject] UnitOfWork unitOfWork
        )
        {
            var updateGameAnnounceBindingModel = await req.Content.ReadAsAsync<UpdateGameAnnounceBindingModel>();

            var announce = await unitOfWork.Repository<GameAnnounce>()
                .GetAsync(a => a.Id == updateGameAnnounceBindingModel.AnnounceId);

            announce.DateUpdated = DateTime.UtcNow;
            announce.Direction = updateGameAnnounceBindingModel.Direction;
            announce.AnnounceTypeString = updateGameAnnounceBindingModel.AnnounceType;

            unitOfWork.Repository<GameAnnounce>().Update(announce);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        /// <summary>
        /// Delete a specific game announce
        /// </summary>
        [FunctionName("DeleteGameAnnounce")]
        public static async Task<HttpResponseMessage> DeleteGameAnnounce(
           [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "game/announce/{announceId}")]HttpRequestMessage req,
           string announceId,
           [Inject] UnitOfWork unitOfWork
        )
        {
            Guid announceIdAsGuid = Guid.Parse(announceId);
            var announce = await unitOfWork.Repository<GameAnnounce>()
                .GetAsync(a => a.Id == announceIdAsGuid);
            announce.IsDeleted = true;
            announce.DateUpdated = DateTime.UtcNow;

            unitOfWork.Repository<GameAnnounce>().Update(announce);
            await unitOfWork.SaveChangesAsync();

            return req.CreateResponse(HttpStatusCode.OK);
        }

        private async static Task<List<GameAnnounce>> SaveGameAnnounce(UnitOfWork unitOfWork, GameAnnounceBindingModel gameBindingModel)
        {
            //TODO: refactor. Game is taken from the database for every single announce
            var game = await unitOfWork.Repository<Game>()
                .GetAll(g => g.Id == gameBindingModel.GameId)
                .Include(g => g.GameAnnounces)
                .FirstOrDefaultAsync();

            if (game == null)
            {
                throw new ArgumentException("Game not found.");
            }

            var playerId = GetPlayerIdByDirection(game, gameBindingModel.Direction);
            if (playerId == Guid.Empty)
            {
                throw new ArgumentException("Player not found.");
            }

            var player = await unitOfWork.Repository<Player>()
                .GetAsync(p => p.Id == playerId);
            if (player == null)
            {
                throw new ArgumentException("Player not found.");
            }

            var newAnnounce = new GameAnnounce()
            {
                AnnounceTypeString = gameBindingModel.AnnounceType,
                Direction = gameBindingModel.Direction,
                SequenceNumber = game.GameAnnounces.Count + 1,
                Player = player,
                RegisteredAt = DateTimeOffset.Now.AddSeconds(-gameBindingModel.RegisterDifference),
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            game.GameAnnounces.Add(newAnnounce);
            unitOfWork.Repository<Game>().Update(game);

            return game.GameAnnounces;
        }

        #endregion

        private static Guid GetPlayerIdByDirection(Game game, Direction direction)
        {
            Guid playerId = Guid.Empty;
            switch (direction)
            {
                case Direction.N:
                    playerId = game.NorthPlayerId;
                    break;
                case Direction.E:
                    playerId = game.EastPlayerId;
                    break;
                case Direction.S:
                    playerId = game.SouthPlayerId;
                    break;
                case Direction.W:
                    playerId = game.WestPlayerId;
                    break;
            }

            return playerId;
        }
    }
}
