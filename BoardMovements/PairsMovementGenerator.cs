﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kibik.Entities.Models;

namespace BoardMovements
{
    public class PairsMovementGenerator : IPairsMovementGenerator
    {
        public ICollection<Game> GenerateSingleWinnerMovement(GameTable[] tables, Round[] rounds, TournamentPair[] pairs)
        {
            if (tables.Length >= 2 && rounds.Length == 6 && pairs.Length == 4)
            {

                ICollection<Game> games = new HashSet<Game>(tables.Length * rounds.Length);
                var pair1 = pairs[0].Pair;
                var pair2 = pairs[1].Pair;
                var pair3 = pairs[2].Pair;
                var pair4 = pairs[3].Pair;

                var table1 = tables[0];
                var table2 = tables[1];

                var roundsOrdered = rounds.OrderBy(r => r.SequenceNumber).ToArray();

                int boardsCount = 6; // :TODO For now hardcoded
                var boards = GenerateBoards(boardsCount, roundsOrdered[0].TournamentId);

                Round firstRound = roundsOrdered[0];
                games.Add(new Game
                {
                    Round = firstRound,
                    SequenceNumber = 1,
                    Table = table1,
                    NorthPlayer = pair1.Player1,
                    EastPlayer = pair2.Player1,
                    SouthPlayer = pair1.Player2,
                    WestPlayer = pair2.Player2,
                    Board = boards[0]
                });

                games.Add(new Game
                {
                    Round = firstRound,
                    SequenceNumber = 1,
                    Table = table2,
                    NorthPlayer = pair3.Player1,
                    EastPlayer = pair4.Player1,
                    SouthPlayer = pair3.Player2,
                    WestPlayer = pair4.Player2,
                    Board = boards[1]
                });

                Round secondRound = roundsOrdered[1];
                games.Add(new Game
                {
                    Round = secondRound,
                    SequenceNumber = 2,
                    Table = table1,
                    NorthPlayer = pair2.Player1,
                    EastPlayer = pair4.Player1,
                    SouthPlayer = pair2.Player2,
                    WestPlayer = pair4.Player2,
                    Board = boards[2]

                });
                games.Add(new Game
                {
                    Round = secondRound,
                    SequenceNumber = 2,
                    Table = table2,
                    NorthPlayer = pair3.Player1,
                    EastPlayer = pair1.Player1,
                    SouthPlayer = pair3.Player2,
                    WestPlayer = pair1.Player2,
                    Board = boards[3]
                });

                Round thirdRound = roundsOrdered[2];
                games.Add(new Game
                {
                    Round = thirdRound,
                    SequenceNumber = 3,
                    Table = table1,
                    NorthPlayer = pair1.Player1,
                    EastPlayer = pair4.Player1,
                    SouthPlayer = pair1.Player2,
                    WestPlayer = pair4.Player2,
                    Board = boards[4]

                });
                games.Add(new Game
                {
                    Round = thirdRound,
                    SequenceNumber = 3,
                    Table = table2,
                    NorthPlayer = pair2.Player1,
                    EastPlayer = pair3.Player1,
                    SouthPlayer = pair2.Player2,
                    WestPlayer = pair3.Player2,
                    Board = boards[5]
                });

                Round fourthRound = roundsOrdered[3];
                games.Add(new Game
                {
                    Round = fourthRound,
                    SequenceNumber = 4,
                    Table = table1,
                    NorthPlayer = pair4.Player1,
                    EastPlayer = pair3.Player1,
                    SouthPlayer = pair4.Player2,
                    WestPlayer = pair3.Player2,
                    Board = boards[0]

                });
                games.Add(new Game
                {
                    Round = fourthRound,
                    SequenceNumber = 4,
                    Table = table2,
                    NorthPlayer = pair2.Player1,
                    EastPlayer = pair1.Player1,
                    SouthPlayer = pair2.Player2,
                    WestPlayer = pair1.Player2,
                    Board = boards[1]
                });

                Round fifthRound = roundsOrdered[4];
                games.Add(new Game
                {
                    Round = fifthRound,
                    SequenceNumber = 5,
                    Table = table1,
                    NorthPlayer = pair1.Player1,
                    EastPlayer = pair3.Player1,
                    SouthPlayer = pair1.Player2,
                    WestPlayer = pair3.Player2,
                    Board = boards[2]

                });
                games.Add(new Game
                {
                    Round = fifthRound,
                    SequenceNumber = 5,
                    Table = table2,
                    NorthPlayer = pair4.Player1,
                    EastPlayer = pair2.Player1,
                    SouthPlayer = pair4.Player2,
                    WestPlayer = pair2.Player2,
                    Board = boards[3]
                });

                Round sixthRound = roundsOrdered[5];
                games.Add(new Game
                {
                    Round = sixthRound,
                    SequenceNumber = 6,
                    Table = table1,
                    NorthPlayer = pair3.Player1,
                    EastPlayer = pair2.Player1,
                    SouthPlayer = pair3.Player2,
                    WestPlayer = pair2.Player2,
                    Board = boards[4]

                });
                games.Add(new Game
                {
                    Round = sixthRound,
                    SequenceNumber = 6,
                    Table = table2,
                    NorthPlayer = pair4.Player1,
                    EastPlayer = pair1.Player1,
                    SouthPlayer = pair4.Player2,
                    WestPlayer = pair1.Player2,
                    Board = boards[5]
                });

                return games;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public ICollection<Game> GenerateTwoWinnerMovement(GameTable[] tables, Round[] rounds, TournamentPair[] pairs)
        {
            throw new NotImplementedException();
        }

        private Board[] GenerateBoards(int boardsCount, int tournamentId)
        {
            Board[] boards = new Board[boardsCount];
            Direction[] directions = new Direction[] { Direction.N, Direction.E, Direction.S, Direction.W };
            Vulnerability[] vulnerabilities = new Vulnerability[] { Vulnerability.None, Vulnerability.NS, Vulnerability.EW, Vulnerability.All };
            for(int i = 0; i < boardsCount; i++)
            {
                Board board = new Board
                {
                    Sequence = i + 1,
                    TournamentId = tournamentId,
                    Dealer = directions[i % 4],
                    IsEastWestInZone = vulnerabilities[i % 4] == Vulnerability.EW || vulnerabilities[i % 4] == Vulnerability.All,
                    IsNorthSouthInZone = vulnerabilities[i % 4] == Vulnerability.NS || vulnerabilities[i % 4] == Vulnerability.All,
                };
                boards[i] = board;

                if(i > 0 && i % 4 == 0) // not first iteration but at its 4-th, 8-th, 12-th etc.
                {
                    var firstVulnerability = vulnerabilities[0];
                    for(int j = 1; j < vulnerabilities.Length; j++)
                    {
                        vulnerabilities[j - 1] = vulnerabilities[j]; // shift all forward except first one which goes to the end
                    }

                    vulnerabilities[vulnerabilities.Length - 1] = firstVulnerability; // put to the end
                }
            }
            return boards;

        }

        private enum Vulnerability
        {
            None,NS,EW,All
        }
    }
}
