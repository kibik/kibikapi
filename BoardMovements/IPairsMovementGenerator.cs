﻿using Kibik.Entities.Models;
using System.Collections.Generic;

namespace BoardMovements
{
    public interface IPairsMovementGenerator
    {
        ICollection<Game> GenerateSingleWinnerMovement(GameTable[] tables, Round[] rounds, TournamentPair[] pairs);
        ICollection<Game> GenerateTwoWinnerMovement(GameTable[] tables, Round[] rounds, TournamentPair[] pairs);
    }
}
