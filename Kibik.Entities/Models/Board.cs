﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class Board
    {
        [Key]
        public int Id { get; set; }

        public int Sequence { get; set; }

        public bool IsNorthSouthInZone { get; set; }
        public bool IsEastWestInZone { get; set; }
        public Direction Dealer { get; set; }

        public int TournamentId { get; set; }

        [ForeignKey("TournamentId")]
        public Tournament Tournament { get; set; }
    }
}
