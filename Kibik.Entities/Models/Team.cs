﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kibik.Entities.Models
{
    public class Team
    {
        public Team()
        {
            TeamPlayers = new HashSet<TeamPlayer>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public virtual ICollection<TeamPlayer> TeamPlayers { get; set; }
    }
}
