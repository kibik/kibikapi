﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class Pair
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid Player1Id { get; set; }
        public Guid Player2Id { get; set; }
        [ForeignKey("Player1Id")]
        public Player Player1 { get; set; }
        [ForeignKey("Player2Id")]
        public Player Player2 { get; set; }
    }
}
