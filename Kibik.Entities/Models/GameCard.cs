﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class GameCard: GeneralDbModel
    {
        private string cardTypeString;

        [Key]
        public Guid Id { get; set; }

        [Required]
        public Direction Direction { get; set; }
        public int SequenceNumber { get; set; }
        public string CardTypeString
        {
            get
            {
                return cardTypeString;
            }
            set
            {
                var cardType = new GameCardType(value);
                this.CardType = cardType;
                cardTypeString = cardType.ToString();
            }
        }

        [NotMapped]
        public GameCardType CardType { get; private set; }
        public Guid PlayerId { get; set; }
        [ForeignKey("PlayerId")]
        public Player Player { get; set; }
        public DateTimeOffset RegisteredAt { get; set; }
    }
}
