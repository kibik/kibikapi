﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class Game : GeneralDbModel
    {
        [Key]
        public Guid Id { get; set; }
        public int RoundId { get; set; }
        [ForeignKey("RoundId")]
        public Round Round { get; set; }
        public int SequenceNumber { get; set; }

        public int GameTableId { get; set; }

        [ForeignKey("GameTableId")]
        public GameTable Table { get; set; }
        public Guid NorthPlayerId { get; set; }
        [ForeignKey("NorthPlayerId")]
        public Player NorthPlayer { get; set; }
        public Guid EastPlayerId { get; set; }
        [ForeignKey("EastPlayerId")]
        public Player EastPlayer { get; set; }
        public Guid SouthPlayerId { get; set; }
        [ForeignKey("SouthPlayerId")]
        public Player SouthPlayer { get; set; }
        public Guid WestPlayerId { get; set; }
        [ForeignKey("WestPlayerId")]
        public Player WestPlayer { get; set; }

        public int EastWestResult { get; set; }
        public int NorthSouthResult { get; set; }

        public int EastWestTricks { get; set; }
        public int NorthSouthTricks { get; set; }

        public string Attack { get; set; }
        public string Contract { get; set; }
        public Direction Caller { get; set; }
        public bool IsFinished { get; set; }

        public int BoardId { get; set; }

        [ForeignKey("BoardId")]
        public Board Board { get; set; }

        public List<GameAnnounce> GameAnnounces { get; set; }

        public List<GameCard> GameCards { get; set; }
    }
}
