﻿using System;
using System.Text.RegularExpressions;

namespace Kibik.Entities.Models
{
    public class GameAnnounceType 
    {
        private readonly Regex regex;
        public GameAnnounceType(string type)
        {
            regex = new Regex(@"(?<rank>\d*)(?<type>\w+)");
            if (regex.IsMatch(type))
            {
                var collection = regex.Match(type).Groups;

                var rank = collection[1].Value;
                if (!string.IsNullOrEmpty(rank))
                {
                    Rank = int.Parse(rank);
                }
                
                Type = Enum.Parse<AnnounceType>(collection[2].Value, true);
            }
        }

        public int? Rank { get; private set; }

        public AnnounceType Type { get; private set; }

        public override string ToString()
        {
            return $"{Rank}{Type}";
        }
    }

    public enum AnnounceType
    {
        Pass, Cl, Di, He, Sp, NT, Dbld, RDbld, Alert
    }
}
