﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class TableCard
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        [ForeignKey("GameId")]
        public Game Game { get; set; }
        public Direction Direction { get; set; }
        public string CardType { get; set; }
    }
}
