﻿using System;

namespace Kibik.Entities.Models
{
    public class TeamPlayer
    {
        public Guid TeamId { get; set; }
        public Team Team { get; set; }
        public Guid PlayerId { get; set; }
        public Player Player { get; set; }
    }
}
