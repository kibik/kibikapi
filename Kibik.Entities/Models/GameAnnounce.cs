﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class GameAnnounce : GeneralDbModel
    {
        private string announceTypeString;

        [Key]
        public Guid Id { get; set; }

        [Required]
        public Direction Direction { get; set; }

        public int SequenceNumber { get; set; }

        public string AnnounceTypeString {
            get
            {
                return announceTypeString;
            }
            set
            {
                var announceType = new GameAnnounceType(value);
                this.AnnounceType = announceType;
                announceTypeString = announceType.ToString();
            }
        }

        [NotMapped]
        public GameAnnounceType AnnounceType { get; private set; }

        public Guid PlayerId { get; set; }

        [ForeignKey("PlayerId")]
        public Player Player { get; set; }

        public DateTimeOffset RegisteredAt { get; set; }
    }
}
