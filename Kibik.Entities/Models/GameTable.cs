﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class GameTable : GeneralDbModel
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Sequence { get; set; }

        public int LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }

    }
}
