﻿using System;

namespace Kibik.Entities.Models
{
    public abstract class GeneralDbModel
    {
        public DateTime DateUpdated { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
    }
}
