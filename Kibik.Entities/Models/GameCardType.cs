﻿using System;
using System.Text.RegularExpressions;

namespace Kibik.Entities.Models
{
    public class GameCardType : IComparable<GameCardType>, IComparable
    {
        private readonly Regex regex;
        public GameCardType(string type)
        {
            regex = new Regex(@"(?<rank>[23456789]{1}|[AKQJ]|10)(?<type>\w+)");
            if (regex.IsMatch(type))
            {

                var collection = regex.Match(type).Groups;

                Rank = collection[1].Value;
                Type = Enum.Parse<CardType>(collection[2].Value, true);
            }
        }

        public string Rank { get; private set; }

        public CardType Type { get; private set; }

        public int CompareTo(GameCardType other)
        {
            var baseCardRank = this.GetCardRank(this);
            var compareCardRank = this.GetCardRank(other);

            return baseCardRank.CompareTo(compareCardRank);
        }

        public int CompareTo(object obj)
        {
            if (obj != null && !(obj is GameCardType))
                throw new ArgumentException("Object must be of type GameCardType.");

            return CompareTo(obj as GameCardType);
        }

        public static bool operator <(GameCardType gameCard1, GameCardType gameCard2)
        {
            return gameCard1.CompareTo(gameCard2) < 0;
        }

        public static bool operator >(GameCardType gameCard1, GameCardType gameCard2)
        {
            return gameCard1.CompareTo(gameCard2) > 0;
        }

        public override string ToString()
        {
            return $"{this.Rank}{this.Type}";
        }

        private int GetCardRank(GameCardType other)
        {
            switch (other.Rank)
            {
                case "J":
                    return 11;
                case "Q":
                    return 12;
                case "K":
                    return 13;
                case "A":
                    return 14;
                default:
                    return int.Parse(other.Rank);
            }
        }
    }

    public enum CardType
    {
        Cl, Di, He, Sp
    }
}
