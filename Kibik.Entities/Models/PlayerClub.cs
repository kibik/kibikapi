﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class PlayerClub
    {
        [Key]
        public int Id { get; set; }
        public Guid PlayerId { get; set; }
        [ForeignKey("PlayerId")]
        public Player Player { get; set; }
        public int ClubId { get; set; }
        [ForeignKey("ClubId")]
        public Club Club { get; set; }
        public DateTime? JoinedOn { get; set; }
        public DateTime? LeftOn { get; set; }
    }
}
