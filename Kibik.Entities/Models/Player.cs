﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class Player : GeneralDbModel
    {
        public Player()
        {
            TeamPlayers = new HashSet<TeamPlayer>();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Country { get; set; }
        public DateTime BirthDate { get; set; }
        public string IBN { get; set; }
        [Required]
        [MaxLength(255)]
        [EmailAddress]
        public string Email { get; set; }
        //TODO: delete password from here
        public string Password { get; set; }
        public int? ClubId { get; set; }
        [ForeignKey("ClubId")]
        public Club Club { get; set; }
        public virtual ICollection<TeamPlayer> TeamPlayers { get; set; }
    }
}
