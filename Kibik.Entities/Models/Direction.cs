﻿namespace Kibik.Entities.Models
{
    public enum Direction
    {
        N, E, S, W
    }
}
