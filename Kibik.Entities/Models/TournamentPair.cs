﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class TournamentPair
    {
        [Key]
        public Guid Id { get; set; }
        public int TournamentId { get; set; }
        [ForeignKey("TournamentId")]
        public Tournament Tournament { get; set; }
        public Guid PairId { get; set; }
        [ForeignKey("PairId")]
        public Pair Pair { get; set; }
    }
}
