﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kibik.Entities.Models
{
    public class Tournament : GeneralDbModel
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int LocationId { get; set; }
        [ForeignKey("LocationId")]
        public Location Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Rounds { get; set; }

        public int Tables { get; set; }

        public int Boards { get; set; }

        public string MovementMethod { get; set; }
        public TournamentType Type { get; set; }
        public bool IsOpen { get; set; }
        public bool IsComplete { get; set; }
        public string Description { get; set; }
        public string Logo { get; set; }
        public string LogoFileName { get; set; }
    }

    public enum TournamentType
    {
        Double, Team
    }
}
